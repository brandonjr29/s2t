import supertest from 'supertest';
import { app, server } from '../src/index';

const api = supertest(app);

// beforeAll(() => {});

test('initial test', async () => {
	await api.get('/',).expect(200);
});

afterAll(() => {
	server.close();
});