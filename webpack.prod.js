const { merge } = require('webpack-merge');
const webpackCommon = require('./webpack.common');

export default merge(webpackCommon, {
	mode: 'production'
});