const  { merge } = require('webpack-merge');
const webpackCommon = require('./webpack.common');
const NodemonPlugin = require('nodemon-webpack-plugin');

module.exports = merge(webpackCommon, {
	mode: 'development',
	devtool: 'inline-source-map',
	plugins: [
		new NodemonPlugin()
	]
});