const path = require('path');
const webpackNodeExternals = require('webpack-node-externals');
const DotenvWebpackPlugin = require('dotenv-webpack');

module.exports = {
	target: 'node',

	resolve: {
		extensions: ['.js', '.ts']
	},

	entry: {
		app: path.resolve(__dirname, 'src', 'index.ts')
	},

	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: 'bundle.js'
	},

	externals: [webpackNodeExternals()],

	module: {
		rules: [
			{
				test: /(js|ts)x?$/,
				exclude: path.resolve(__dirname, 'node_modules'),
				loader: 'ts-loader'
			}
		]
	},

	plugins: [
		new DotenvWebpackPlugin({
			systemvars: true
		})
	]
}

