import express from 'express';

const app = express();
const port: string | number = process.env.PORT || 3000;

app.get('/', (_req: Request, res: any) => {
	res.send('Server is ready');
});

const server = app.listen(port, () => {
	console.log(`Server is listening in port ${port}`);
	console.log(process.env.TEST_ENV);
});

export { app, server };